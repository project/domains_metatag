<?php
/**
 * Implements hook_metatag_config_default().
 */
function domains_metatag_metatag_config_default() {
  $configs     = array();
  
  $domains = _domains_metatag_get_active_domains();
  
  foreach ($domains as $domain) {
    $config = new stdClass();
    $config->instance = 'global:' . $domain['machine_name'] . ' frontpage';
    $config->api_version = 1;
    $config->disabled = FALSE;
    $config->config = array(
      'title' => array('value' => variable_get('site_slogan') ? '[site:name] | [site:slogan]' : '[site:name]'),
      'canonical' => array('value' => '[site:url]'),
    );
  }
  
  return $configs;
}

/**
 * Implements hook_metatag_config_instance_info().
 */
function domains_metatag_metatag_config_instance_info() {
  $domains = _domains_metatag_get_active_domains();
  $info = array();
  $domain['domain']  = array('label' => t('Domain')); 
  foreach ($domains as $domain) {
    $info[$domain['machine_name']]  = array('label' => $domain['sitename']);  
    $info[$domain['machine_name'] . ':frontpage'] = array('label' => t($domain['sitename'] . ' - Front page'));
    $info[$domain['machine_name'] . ':403'] = array('label' => t($domain['sitename'] . ' - 403 access denied'));
    $info[$domain['machine_name'] . ':404'] = array('label' => t($domain['sitename'] . ' - 404 access denied'));

    //Add instance information for entities.
    $entity_types = entity_get_info();
    foreach ($entity_types as $entity_type => $entity_info) {
      if (metatag_entity_supports_metatags($entity_type)) {
        $info[$domain['machine_name'] . ':' . $entity_type] = array('label' => t($domain['sitename'] . ' - ' . $entity_info['label'])); 
        foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
          if (count($entity_info['bundles'] == 1) && $bundle == $entity_type) {
            // Skip default bundles (entities do not really have bundles).
            continue;
          }
          if (metatag_entity_supports_metatags($entity_type, $bundle)) {
            $info[$domain['machine_name'] . ':' . $entity_type . ':' . $bundle] = array('label' => t($domain['sitename'] . ' - ' . $bundle_info['label']));
          }
        }
      }
    }
  }
  return $info;
}

/**
 * Implements hook_page_build().
 */
function domains_metatag_page_build(&$page) {
  if (path_is_admin(current_path()) && !variable_get('metatag_tag_admin_pages', FALSE)) {
    return;
  }
  if (module_exists('domain')) {
    $current_domain = domain_get_domain();

    if (drupal_is_front_page()) {
      $instance = $current_domain['machine_name'] . ':frontpage';
    }
    else {
      $instance_config = array(
        'user' => array(
          'type' => 'user',
          'depth' => 1,
          'bundle' => 'user',
        ),
        'taxonomy' => array(
          'type' => 'taxonomy_term',
          'depth' => 2,
          'bundle' => 'vocabulary_machine_name',
        ),
        'node' => array(
          'type' => 'node',
          'depth' => 1,
          'bundle' => 'type'
        ),
      );
      $arg = arg(0);
      if (!empty($instance_config[$arg])) {
        $entity = menu_get_object($instance_config[$arg]['type'], $instance_config[$arg]['depth']);
        if($arg == 'user') {
          $instance = $current_domain['machine_name'] . ':' . $instance_config[$arg]['type'] . ':' . $instance_config[$arg]['bundle'];
        }
        else {
          $instance = $current_domain['machine_name'] . ':' . $instance_config[$arg]['type'] . ':' . $entity->$instance_config[$arg]['bundle'];
        }
      }
      else {
        return 0;
      }
    }

    $cid_parts = array(
      'instance' => $instance,
    );
    $cid = metatag_cache_default_cid_parts($cid_parts);

    if ($cache = metatag_cache_get($cid)) {
      $metatags = $cache->data;
    }
    else {
      $metatags = metatag_metatags_view($instance, array());
      metatag_cache_set($cid, $metatags);
    }

    $page['content']['metatags'][$instance] = $metatags;
  }
}

/**
 *Get active domains from domain module.  
 */
function _domains_metatag_get_active_domains() {
 
  $domains = domain_domains();
  $active_domains = array();
  
  foreach ($domains as $domain) {
    if ($domain['valid'] == 1 && $domain['domain_id'] != 1) {
      $active_domains[] = $domain;
    }
  }
  
  ksort($active_domains, SORT_NUMERIC);
  return $active_domains;
}